export type OperatorType = {
    id: string;
    name: string;
    phoneMask: string;
  };