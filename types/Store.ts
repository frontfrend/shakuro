import { OperatorType } from "./Operator";

export interface IApiStore {
  operatorStore: {
    operator: OperatorType | null;
    isFetching: boolean;
    error?: string | null;
  };
  operatorsStore: {
    operators: OperatorType[];
    isFetching: boolean;
  };
  refillStore: {
    error: string | null;
    isFetching: boolean;
  }
}
