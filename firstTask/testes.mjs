import func from "./func.mjs"
import lastMaxEntryOfTwoImprovements from "./maxFirstEntryImprovements.mjs"
import lastMaxEntryOfTwoRewrite from "./maxFirstEntry.mjs"
 
console.log('--------------')
console.log(func("qwertyu", "r", "y"))
console.log(lastMaxEntryOfTwoImprovements("qwertyu", "r", "y"))
console.log(lastMaxEntryOfTwoRewrite("qwertyu", "r", "y"))
console.log('--------------')


console.log(func("", "r", "y"))
console.log(lastMaxEntryOfTwoImprovements("", "r", "y"))
console.log(lastMaxEntryOfTwoRewrite("", "r", "y"))

console.log('--------------')
console.log(func("", "r", "y"))
console.log(lastMaxEntryOfTwoImprovements("", "r", "y"))
console.log(lastMaxEntryOfTwoRewrite("", "r", "y"))

console.log('--------------')
console.log(func("gggg", "r", "y"))
console.log(lastMaxEntryOfTwoImprovements("gggg", "r", "y"))
console.log(lastMaxEntryOfTwoRewrite("gggg", "r", "y"))

console.log('--------------')
console.log(func("gggg", "g", "g"))
console.log(lastMaxEntryOfTwoImprovements("gggg", "g", "g"))
console.log(lastMaxEntryOfTwoRewrite("gggg", "g", "g"))
console.log('--------------')