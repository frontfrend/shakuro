const isEmpty = (s) => s.match(/^$/)

function lastMaxEntryOfTwoImprovements(s, a, b) {
	if (isEmpty(s)) {
		return -1;
	}
	
	let i = s.length -1;
	let aIndex =     -1;
	let bIndex =     -1;
	
	while ((aIndex == -1) && (bIndex == -1) && (i > 0)) {
	    if (s.substring(i, i +1) == a) {
	    	aIndex = i;
    	}
	    if (s.substring(i, i +1) == b) {
	    	bIndex = i;
    	}
	    i = i - 1;
	}
	
	return Math.max(aIndex, bIndex)
}
 
export default lastMaxEntryOfTwoImprovements