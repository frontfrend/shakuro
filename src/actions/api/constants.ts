export const SET_OPERATOR_RESPONSE = "operator/response";
export const SET_OPERATOR_ERROR = "operator/error";
export const SET_OPERATOR_FETCHING_STATE = "operator/fetching";

export const SET_OPERATORS_RESPONSE = "operators/response";
export const SET_OPERATORS_FETCHING_STATE = "operators/fetching";


export const SET_REFILL_ERROR = "refill/error";
export const SET_REFILL_FETCHING_STATE = "refill/fetching";

