import { getOperatorsList } from "../../api";
import { store } from "../../store/api";
import {
  SET_OPERATORS_RESPONSE,
  SET_OPERATORS_FETCHING_STATE
} from "./constants";

export const fetchOperators = async () => {
  try {
    const operatorPromise = getOperatorsList();

    store.dispatch(SET_OPERATORS_FETCHING_STATE, true);
    const jsonResponse = await operatorPromise;

    store.dispatch(SET_OPERATORS_RESPONSE, jsonResponse.operators);
  }
  // catch (e) {
  //     store.dispatch(SET_OPERATORS_ERROR, e.message);
  // }
  finally {
    store.dispatch(SET_OPERATORS_FETCHING_STATE, false);
  }
};
