import { getOperator } from "../../api";
import { store } from "../../store/api";
import {
  SET_OPERATOR_FETCHING_STATE,
  SET_OPERATOR_RESPONSE,
  SET_OPERATOR_ERROR
} from "./constants";

export const fetchOperator = async (id: string) => {
  try {
    const operatorPromise = getOperator(id);

    store.dispatch(SET_OPERATOR_FETCHING_STATE, true);
    const jsonResponse = await operatorPromise;

    store.dispatch(SET_OPERATOR_RESPONSE, jsonResponse);
  } catch (e) {
    store.dispatch(SET_OPERATOR_ERROR, e.message);
  } finally {
    store.dispatch(SET_OPERATOR_FETCHING_STATE, false);
  }
};
