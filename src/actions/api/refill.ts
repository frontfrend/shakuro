import { requestRefill } from "../../api";
import { store } from "../../store/api";
import {
  SET_REFILL_FETCHING_STATE,
  SET_REFILL_ERROR
} from "./constants";
import { navigate } from "hookrouter";

export const sendRefillRequest = async () => {
  try {
    const operatorPromise = requestRefill();

    store.dispatch(SET_REFILL_FETCHING_STATE, true);
    await operatorPromise;

    store.dispatch(SET_REFILL_ERROR, null);

    navigate("/");
  } catch (e) {
    store.dispatch(SET_REFILL_ERROR, e.message);
  } finally {
    store.dispatch(SET_REFILL_FETCHING_STATE, false);
  }
   
};

