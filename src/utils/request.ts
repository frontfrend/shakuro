export async function request<T>(
    url: string,
    request: RequestInit
): Promise<T> {
    const response = await fetch(url, request);

    
    const body = await response.json();

    if(![200, 201].includes(response.status)) {
        throw new Error(body.message)
    }
    return body;
}