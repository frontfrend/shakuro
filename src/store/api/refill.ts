import { StoreonStore } from "storeon";

import { SET_REFILL_ERROR, SET_REFILL_FETCHING_STATE } from "../../actions/api/constants";
import { IApiStore } from "../../../types/Store";

const refillStore = (store: StoreonStore<IApiStore>) => {
  store.on("@init", () => ({
    refillStore: { error: null, isFetching: false }
  }));

  store.on(SET_REFILL_ERROR, ({ refillStore }, error) => {
    return { refillStore: { ...refillStore, error } };
  });

  store.on(SET_REFILL_FETCHING_STATE, ({ refillStore }, isFetching) => {
    return { refillStore: { ...refillStore, isFetching } };
  });
};

export default refillStore;
