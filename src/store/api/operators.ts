import { StoreonStore } from "storeon";

import {
  SET_OPERATORS_FETCHING_STATE,
  SET_OPERATORS_RESPONSE
} from "../../actions/api/constants";
import { IApiStore } from "../../../types/Store";

const operatorsStore = (store: StoreonStore<IApiStore>) => {
  store.on("@init", () => ({
    operatorsStore: { operators: [], isFetching: false }
  }));

  store.on(SET_OPERATORS_RESPONSE, ({ operatorsStore }, operators) => {
    return { operatorsStore: { ...operatorsStore, operators } };
  });

  store.on(SET_OPERATORS_FETCHING_STATE, ({ operatorsStore }, isFetching) => {
    return { operatorsStore: { ...operatorsStore, isFetching } };
  });
};

export default operatorsStore;
