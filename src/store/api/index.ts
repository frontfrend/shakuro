import { createStoreon } from 'storeon'
import operatorStore from './operator'
import { storeonDevtools } from 'storeon/devtools';
import operatorsStore from './operators';
import refillStore from './refill';

export const store = createStoreon([operatorStore, operatorsStore, refillStore, process.env.NODE_ENV !== 'production' && storeonDevtools])
