import { StoreonStore } from "storeon";

import {
  SET_OPERATOR_RESPONSE,
  SET_OPERATOR_ERROR,
  SET_OPERATOR_FETCHING_STATE
} from "../../actions/api/constants";
import { IApiStore } from "../../../types/Store";

const operatorStore = (store: StoreonStore<IApiStore>) => {
  store.on("@init", () => ({
      operatorStore: { operator: null, isFetching: false, error: null }
  }));

  store.on(SET_OPERATOR_RESPONSE, ({ operatorStore }, operator) => {
    return { operatorStore: { ...operatorStore, operator } };
  });

  store.on(SET_OPERATOR_ERROR, ({ operatorStore }, error) => {
    return { operatorStore: { ...operatorStore, error } };
  });

  store.on(SET_OPERATOR_FETCHING_STATE, ({ operatorStore }, isFetching) => {
    return { operatorStore: { ...operatorStore, isFetching } };
  });
};

export default operatorStore;
