import { request } from '../utils/request'
import { OperatorType } from '../../types/Operator'
 
export interface IOperatorsResponse {
    operators: OperatorType[]
}
 

export const getOperatorsList = () => request<IOperatorsResponse>('/api/operators', { method: 'get' } )
export const getOperator = (id: string) => request<OperatorType>(`/api/operators/${id}`, { method: 'get' } )