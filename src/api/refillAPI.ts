import { request } from '../utils/request'
export interface RefillResponse {
    
}

export const requestRefill = () => request<RefillResponse>('/api/refill', { method: 'post' } )
 