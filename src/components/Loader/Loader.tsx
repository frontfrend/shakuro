import React from "react";

import style from "./Loader.scss";

const Loader = () => {
  return (
    <div className={style.container}>
      <div className={style.loader}/>
      <span  className={style.loader__text}>Loading</span>
    </div>
  );
};

export default Loader;
