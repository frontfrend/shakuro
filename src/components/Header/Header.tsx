import React from "react";

import style from "./Header.scss";

const Header = ({ children }: { children: React.ReactNode }) => {
  return (
    <h2 className={style.header}>{children}
    </h2>
  );
};

export default Header;
