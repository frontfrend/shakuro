import React from "react";
import { useRoutes, HookRouter } from "hookrouter";
import { StoreContext } from 'storeon/react'

import "./Server";
import NotFound from "./pages/NotFound/NotFound";
import OperatorsList from "./pages/OperatorsList/OperatorsList";
import RefillBalance from "./pages/RefillBalance/RefillBalance";
import Layout from "./Layout/Layout";
import { store } from "./store/api";

const routes = {
  "/": () => <OperatorsList />,
  "/refill/:operatorId": ({ operatorId }: HookRouter.QueryParams) => (
    <RefillBalance operatorId={operatorId} />
  )
};

export default function App() {
  const routeResult = useRoutes(routes);

  return <StoreContext.Provider value={store}><Layout>{routeResult || <NotFound />}</Layout></StoreContext.Provider>;
}
