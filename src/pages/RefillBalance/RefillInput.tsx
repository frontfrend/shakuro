import React, { useMemo, useEffect, useState, useCallback } from "react";
import { IMaskInput } from "react-imask";
import style from "./RefillBalance.scss";

export type RefillInputProps = {
  showError: boolean;
  error: string;
  id: string;
  [x: string]: any
};

const RefillInput = ({ showError, error, id, lebel, ...rest }: RefillInputProps) => (
  <>
    <label htmlFor={id}>{lebel}</label>
    <IMaskInput id={id}  {...rest} />
    {showError && (
      <label htmlFor={id} className={`${style.form__item} ${style.error}`}>
        {error}
      </label>
    )}
  </>
);

export default RefillInput;
