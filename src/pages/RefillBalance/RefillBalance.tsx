import React, { useMemo, useEffect, useState, useCallback } from "react";
import Loader from "../../components/Loader/Loader";
import { IMaskInput } from "react-imask";
import style from "./RefillBalance.scss";
import Header from "../../components/Header/Header";
import { useStoreon } from "storeon/react";
import { fetchOperator } from "../../actions/api/operator";
import { IApiStore } from "../../../types/Store";
import { sendRefillRequest } from "../../actions/api/refill";
import RefillInput from "./RefillInput";

export type RefillBalanceProps = {
  operatorId: string;
};

const RefillBalance = ({ operatorId }: RefillBalanceProps) => {
  const { operatorStore, operatorsStore, refillStore } = useStoreon<IApiStore>(
    "operatorStore",
    "operatorsStore",
    "refillStore"
  );
  const { operator, error, isFetching } = operatorStore;
  const { error: serverError, isFetching: refilIsFetching } = refillStore;
  const { operators } = operatorsStore;

  const [showError, setErrorVisibility] = useState(false);

  const [phone, changePhone] = useState();
  const [isPhoneCompleted, changePhoneCompleted] = useState();

  const [summ, changeSumm] = useState();
  const [isSummCompleted, changeSummCompleted] = useState();

  const viewedOperator = useMemo(() => {
    return operators.find(item => item.id === operatorId) || operator;
  }, [operators, operator]);

  const onSubmitForm = useCallback(
    (e: React.FormEvent<EventTarget>) => {
      e.preventDefault();

      if (isPhoneCompleted && isSummCompleted) {
        sendRefillRequest();
        return;
      }
      setErrorVisibility(true);
    },
    [isPhoneCompleted, isSummCompleted, setErrorVisibility]
  );
  useEffect(() => {
    if (!error && !viewedOperator) {
      fetchOperator(operatorId);
    }
  }, [viewedOperator, error]);

  if (error) {
    return <div className={style.criticalError}>{error}</div>;
  }

  if (isFetching || !viewedOperator || refilIsFetching) {
    return <Loader />;
  }

  return (
    <form id="rf_form" className={style.form} onSubmit={onSubmitForm}>
      <Header>Operator: {viewedOperator?.name}</Header>
      <fieldset className={style.form__fields}>
        <legend className={style.form__legend}>Data for a refill balance</legend>
        <RefillInput
          lebel="Phone number: "
          id="rf_phone_number"
          error="Please, fill the phone field"
          showError={showError && !isPhoneCompleted}
          unmask="typed"
          value={phone}
          mask={viewedOperator?.phoneMask}
          onComplete={() => {
            changePhoneCompleted(true);
          }}
          aria-required="true"
          onAccept={(value: string) => {
            changePhone(value);
            if (isPhoneCompleted) {
              changePhoneCompleted(false);
            }
            if (showError) {
              setErrorVisibility(false);
            }
          }}
          type="tel"
          className={style.form__item}
          placeholder="Enter number here"
        />

        <RefillInput
          lebel="Amount:"
          id="rf_amount"
          error="Please, fill the amount of refill field"
          showError={showError && !isSummCompleted}
          mask={Number}
          min={1}
          type="number"
          aria-required="true"
          max={1000}
          value={summ}
          onComplete={(maskValue: string) => {
            if (maskValue) {
              changeSummCompleted(true);
            }
          }}
          className={style.form__item}
          onAccept={(value: string) => {
            changeSumm(value);
            if (isSummCompleted) {
              changeSummCompleted(false);
            }
            if (showError) {
              setErrorVisibility(false);
            }
          }}
        />
      </fieldset>
      <button
        type="submit"
        name="submit"
        value="Submit refill balance"
        className={style.form__item}
        disabled={showError}
      >
        Submit
      </button>

      {serverError && (
        <label
          htmlFor="rf_form"
          className={`${style.form__item} ${style.error}`}
        >
          {serverError}
        </label>
      )}
    </form>
  );
};

export default RefillBalance;
