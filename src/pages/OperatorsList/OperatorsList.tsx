import React, { useContext, useEffect, memo } from "react";
import { A } from "hookrouter";
import Loader from "../../components/Loader/Loader";
import Header from "../../components/Header/Header";
import { useStoreon } from "storeon/react";
import { IApiStore } from "../../../types/Store";
import { fetchOperators } from "../../actions/api/operators";

const OperatorsList = memo(() => {
  const { operatorsStore } = useStoreon<IApiStore>("operatorsStore");
  const { operators, isFetching } = operatorsStore;
  useEffect(() => {
    if (!operators.length) {
      fetchOperators();
    }
  }, []);

  if (isFetching) {
    return <Loader />;
  }
  return (
    <div>
      <Header>Operators List</Header>
      <ul>
        {!isFetching &&
          operators.map(operator => (
            <li key={operator.id}>
              <A
                href={`/refill/${operator.id}`}
                className="text-blue-500 hover:text-blue-800"
              >
                {operator.name}
              </A>
            </li>
          ))}
      </ul>
    </div>
  );
});

export default OperatorsList;
