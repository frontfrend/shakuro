import React from "react";

import style from "./layout.scss";
 
const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div className={style.layout}>
      {children}
    </div>
  );
};

export default Layout;
