import { Server , Response } from "miragejs";
const operators = [
  { id: "1", name: "Megafon", phoneMask: "+{7}(000)000-00-00" },
  { id: "2", name: "MTS" , phoneMask: "+{7} 000 000-00-00"},
  { id: "3", name: "MGTS", phoneMask: "+{7} 000 000-00-00" },
  { id: "43", name: "Vodaphone", phoneMask: "+{1} 000 000-00-00" }
];
new Server({
  routes() {
    this.namespace = "/api";

    this.get(
      "/operators",
      () => ({
        operators
      }),
      { timing: Math.random() * 1500 }
    );

    this.get(
      "/operators/:id",
      (_: any, request: any) => {
        const operator = operators.find(item => item.id === request.params.id);
        if (operator) {
          return operator;
        }
        return new Response(400, { "Content-Type": 'application/json' }, { message: 'Entity not found!' });
        
      },
      { timing: Math.random() * 1500 }
    );

    this.post(
      "/refill",
      () => {
        const showError = Math.random() * 10 > 5;

        if(showError) {
            return new Response(500, { "Content-Type": 'application/json' }, { message: 'Sorry, but you are not lucky.🎯 Try again!' });
        }

        return {}
      },
      { timing: Math.random() * 1500 }
    );
  }
});
